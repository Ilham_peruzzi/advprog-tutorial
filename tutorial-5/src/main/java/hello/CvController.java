package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                                   String visitor, Model model) {
        if (visitor != null) {
            model.addAttribute("visitor", visitor + ", I hope you interested to hire me");
        } else {
            model.addAttribute("visitor", "This is my CV");
        }
        model.addAttribute("name","Muhammad Ilham Peruzzi");
        model.addAttribute("birthInfo","Serang, 09 June 1998");
        model.addAttribute("address",
                "Link. Martapura RT/RW 002/003 Masigit, Jombang, Cilegon, Banten");
        model.addAttribute("sd","SD Negeri IX Cilegon");
        model.addAttribute("sdYear","2004-2010");
        model.addAttribute("smp","SMP Negeri 3 Cilegon");
        model.addAttribute("smpYear","2010-2013");
        model.addAttribute("sma","SMA Negeri 1 Cilegon");
        model.addAttribute("smaYear","2013-2016");
        model.addAttribute("univ","University of Indonesia");
        model.addAttribute("univYear","2016-Now");
        return "cv";
    }
}
