package applicant;

import java.util.function.Predicate;

public class EmploymentEvaluator extends EvaluatorChain {

    public EmploymentEvaluator(Evaluator next) {
        super(next);
    }

    public Predicate<Applicant> evaluate(Applicant applicant) {
        return theApplicant -> theApplicant.getEmploymentYears() > 0;
    }
}
