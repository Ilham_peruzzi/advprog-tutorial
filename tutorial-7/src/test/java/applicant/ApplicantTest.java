package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!

    static Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }

    @Test
    public void testCreditQualifiedApplicant() {
        boolean result = applicant.evaluate(applicant,
                new CreditEvaluator(
                        new QualifiedEvaluator()));
        assertTrue(result);
    }

    @Test
    public void testCreditAndEmploymentQualifiedApplicant() {
        boolean result = applicant.evaluate(applicant,
                new CreditEvaluator(
                        new EmploymentEvaluator(
                                new QualifiedEvaluator())));
        assertTrue(result);
    }

    @Test
    public void testCriminalAndEmploymentQualifiedApplicant() {
        boolean result = applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new EmploymentEvaluator(new QualifiedEvaluator())));
        assertFalse(result);
    }

    @Test
    public  void testCriminalAndCreditAndEmploymentQualifiedApplicant() {
        boolean result = applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new CreditEvaluator(
                                new EmploymentEvaluator(new QualifiedEvaluator()))));
    }

}
