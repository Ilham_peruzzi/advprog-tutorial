import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    private static Map<String, Integer> data = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        data.put("Alice",10);
        data.put("Bob",11);
        data.put("Charlie",12);
        data.put("Darine",10);
        data.put("Edward",11);
    }

    @Test
    public void testMapGroupCorrectly() {
        Map<Integer, List<String>> groupData = new HashMap<>();
        groupData = ScoreGrouping.groupByScores(data);
        assertEquals(2,groupData.get(10).size());
        assertTrue(groupData.get(10).contains("Alice"));
        assertTrue(groupData.get(10).contains("Darine"));
        assertFalse(groupData.get(10).contains("Bob"));
    }

}