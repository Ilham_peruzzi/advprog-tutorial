package tutorial.javari.animal;

import java.util.ArrayList;
import java.util.List;

public class AnimalList {
    private List<Animal> animals;

    public AnimalList(){
        this.animals = new ArrayList<>();
    }

    public void addAnimal(Animal a){
        this.animals.add(a);
    }

    public Animal getAnimal(int id){
        for (Animal a : this.animals){
            if(a.getId() == id){
                return a;
            }
        }
        return null;
    }

    public List<Animal> getListAnimal(){
        return this.animals;
    }
}
