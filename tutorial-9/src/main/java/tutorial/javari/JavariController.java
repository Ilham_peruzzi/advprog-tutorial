package tutorial.javari;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalList;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class JavariController {
    // TODO Implement me!

    //get list of animals
    @RequestMapping("/javari")
    public AnimalList animalList() {
        return getAnimalList();
    }

    public AnimalList getAnimalList(){
        String csvFile = "tutorial-9/animals_records.csv";
        String line = "";
        AnimalList listOfAnimal = new AnimalList();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] animals = line.split(",");
                Animal animal = new Animal(Integer.parseInt(animals[0])
                        ,animals[1],animals[2], Gender.parseGender(animals[3])
                        ,Double.parseDouble(animals[4]), Double.parseDouble(animals[5])
                        , Condition.parseCondition(animals[6]));
                listOfAnimal.addAnimal(animal);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfAnimal;
    }

    //get information of an animal
    @RequestMapping(value = {"/javari/{id}"}, method= RequestMethod.GET)
    public Animal getAnimal(@PathVariable int id){
        return getAnimalList().getAnimal(id);
    }

    //delete animal from database
    @RequestMapping(value = {"/javari/{id}"}, method = RequestMethod.DELETE)
    public Animal deleteAnimal(@PathVariable int id) {
        Animal deletedAnimal = getAnimalList().getAnimal(id);
        String csvFile = "tutorial-9/animals_records.csv";
        File file = new File(csvFile);
        try{
        List<String> out = Files.lines(file.toPath())
                .filter(line -> Integer.parseInt(line.split(",")[0])!=id)
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return deletedAnimal;
    }

    //add animal to database
    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public Animal addAnimal(@RequestBody String JSON) throws JSONException {
        JSONObject json = new JSONObject(JSON);
        Animal animal = new Animal(json.getInt("id"), json.getString("type")
                , json.getString("name"), Gender.parseGender(json.getString("gender"))
                , json.getDouble("length"), json.getDouble("weight")
                , Condition.parseCondition(json.getString("condition")));
        String[] attrs = {animal.getId().toString(), animal.getType(), animal.getName()
                , animal.getGender().toString(), String.valueOf(animal.getLength())
                , String.valueOf(animal.getWeight()), animal.getCondition().toString()};
        String newAnimal = String.join(",", attrs);
        String csvFile = "tutorial-9/animals_records.csv";
        File file = new File(csvFile);
        try{
            List<String> out = Files.lines(file.toPath())
                    .collect(Collectors.toList());
            out.add(newAnimal);
            Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return animal;
    }

}