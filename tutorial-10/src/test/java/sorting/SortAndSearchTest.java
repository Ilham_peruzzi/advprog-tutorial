package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here

    int[] arrayOfInt;

    @Before
    public void setUp() {
        arrayOfInt = new int[]{7,2,9,8,1,3,5,4,6};
    }

    @Test
    public void testNewSort() {
        assertNotEquals(arrayOfInt[0],1);
        arrayOfInt = Sorter.newSort(arrayOfInt);
        assertEquals(arrayOfInt[0],1);
        for (int i = 0; i < 9; i++) {
            assertEquals(arrayOfInt[i],i + 1);
        }
    }

    @Test
    public void testNewSearch() {
        arrayOfInt = Sorter.newSort(arrayOfInt);
        assertEquals(Finder.newSearch(arrayOfInt, 10),-1);
        assertNotEquals(Finder.newSearch(arrayOfInt, 2), -1);
    }
}
