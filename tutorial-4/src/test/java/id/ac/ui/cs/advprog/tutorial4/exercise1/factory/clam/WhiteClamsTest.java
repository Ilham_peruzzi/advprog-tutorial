package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class WhiteClamsTest {
    private WhiteClams whiteClams;

    @Before
    public void setUp() {
        whiteClams = new WhiteClams();
    }

    @Test
    public void testToString() {
        assertEquals("White Clams from Chesapeake Bay", whiteClams.toString());
    }
}
