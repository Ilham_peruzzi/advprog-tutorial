package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;

public class Cucumber extends Filling {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
        this.description = food.getDescription() + ", adding cucumber";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.food.cost() + .40;
    }
}