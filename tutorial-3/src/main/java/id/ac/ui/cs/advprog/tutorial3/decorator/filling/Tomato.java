package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;

public class Tomato extends Filling {
    Food food;

    public Tomato(Food food) {
        //TODO Implement
        this.food = food;
        this.description = food.getDescription() + ", adding tomato";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.food.cost() + .50;
    }
}